import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.apache.commons.io.FileUtils;


import java.io.File;
import java.io.IOException;

public class Screenshot {

    private WebDriver driver;

    public Screenshot(WebDriver driver){
        this.driver = driver;
    }

    public void makeScreenShot(String fileWithPath) {
        File screenshotFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(screenshotFile, new File("screenshot.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
