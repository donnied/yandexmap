import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class YandexMapApp {

    LoopTask task = new LoopTask();
    Timer timer = new Timer("TaskName");

    public static void main( String[] args )
    {
        YandexMapApp executingTask = new YandexMapApp();
        executingTask.start(convertStringToLong(args[0]));
    }

    private static Long convertStringToLong(String minute){
        return Long.parseLong(minute);
    }

    public void start(Long delay) {
        timer.cancel();
        timer = new Timer("TaskName");
        Date executionDate = new Date();
        timer.scheduleAtFixedRate(task, executionDate, delay * 60 * 1000);
    }

    private class LoopTask extends TimerTask {
        public void run() {
            FindPath findPath = new FindPath();
            findPath.setUp();
            String minimalRouteTime = findPath.buildPath();
            findPath.makeScreenshot();
            findPath.saveLogs(DateAndTime.getCurrentTime(), minimalRouteTime);
            findPath.tearDown();
        }
    }


}
