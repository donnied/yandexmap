import org.openqa.selenium.*;

public class YandexMapMainPage {

    private WebDriver driver;

    public YandexMapMainPage(WebDriver driver){
        this.driver = driver;
    }

    public YandexMapMainPage clickFindWayButton(){
        WebElement findWayButton = driver.findElement(By.xpath("//div[@class='small-search-form-view__icon _type_route']"));
        findWayButton.click();
        return new YandexMapMainPage(driver);
    }

    public YandexMapMainPage setStartingPoint(String startingPoint){
        WebElement findWayButton = driver.findElement(By.xpath("//input[@placeholder='Укажите точку отправления']"));
        findWayButton.sendKeys(startingPoint);
        findWayButton.sendKeys(Keys.RETURN);
        return new YandexMapMainPage(driver);
    }

    public YandexMapMainPage setFinishPoint(String destinationPoint){
        WebElement findWayButton = driver.findElement(By.xpath("//input[@placeholder='Укажите точку назначения']"));
        findWayButton.sendKeys(destinationPoint);
        findWayButton.sendKeys(Keys.RETURN);
        return new YandexMapMainPage(driver);
    }

    public String getMinimalRouteTime(){
        WebElement minimalRouteTime = driver.findElement(By.xpath("(//div[@class='driving-route-form-view__route-title-primary'])[1]"));
        return minimalRouteTime.getText();
    }

    public YandexMapMainPage waitForMapLoad(){
        //driver.findElement(By.xpath("//div[@class='waypoint-pin-view__info']"));
        ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return this;
    }
}
