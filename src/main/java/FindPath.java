import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;



public class FindPath {

    private WebDriver driver;
    private final String startingPoint = "Казанский кафедральный собор";
    private final String destinationPoint = "Воскресенский Смольный собор";

    public String buildPath(){
        driver.get("https://yandex.ru/maps/");
        YandexMapMainPage yandexMapMainPage = new YandexMapMainPage(driver);
        yandexMapMainPage.clickFindWayButton()
                .setStartingPoint(startingPoint)
                .setFinishPoint(destinationPoint)
                .waitForMapLoad();
        return yandexMapMainPage.getMinimalRouteTime();
    }

    public void makeScreenshot(){
        Screenshot screenshot = new Screenshot(driver);
        screenshot.makeScreenShot("screenshot.png");
    }

    public void saveLogs(String currentTime, String minimalRouteTime){
        LogToCSV.saveLog(currentTime, minimalRouteTime);
    }


    public void setUp() {
        System.out.println("Start");
        String os = System.getProperty("os.name").toLowerCase();

        if (os.indexOf("win") >= 0){
            //System.setProperty("webdriver.chrome.driver", "src\\main\\resources\\chromedriver.exe");
            System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        }
        else if (os.indexOf("nux") >= 0 || os.indexOf("nix") >= 0){
            //System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver_linux");
            System.setProperty("webdriver.chrome.driver", "chromedriver_linux");
        }
        else if (os.indexOf("mac") >= 0){
            //System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver");
            System.setProperty("webdriver.chrome.driver", "chromedriver");
        }
        else{
            System.out.println("Неизвестная ОС: " + os);
        }

        driver = new ChromeDriver();
        //driver.manage().window().setSize( new Dimension(800, 400));
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }



    public void tearDown() {
        driver.quit();
        System.out.println("End");
    }


}
