import java.text.SimpleDateFormat;
import java.util.Date;

public class DateAndTime {

    public static String getCurrentTime(){
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
        return formatter.format(date);
    }
}
