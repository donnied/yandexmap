import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class LogToCSV {

    private static FileWriter fileWriter;

    public static void saveLog(String currentTime, String minimalRouteTime) {

        try {
            fileWriter = new FileWriter("csv_log.csv", true);
            fileWriter.append(currentTime);
            fileWriter.append(";");
            fileWriter.append(minimalRouteTime);
            fileWriter.append("\n");
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
